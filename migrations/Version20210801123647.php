<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210801123647 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE Gift (id INT AUTO_INCREMENT NOT NULL, receiver_id INT NOT NULL, stock_id INT NOT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', code VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, price DOUBLE PRECISION NOT NULL, INDEX IDX_44E3633CD53EDB6 (receiver_id), INDEX IDX_44E3633DCD6110 (stock_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Receiver (id INT AUTO_INCREMENT NOT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', firstName VARCHAR(255) NOT NULL, lastName VARCHAR(255) NOT NULL, countryCode VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Stock (id INT AUTO_INCREMENT NOT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE Gift ADD CONSTRAINT FK_44E3633CD53EDB6 FOREIGN KEY (receiver_id) REFERENCES Receiver (id)');
        $this->addSql('ALTER TABLE Gift ADD CONSTRAINT FK_44E3633DCD6110 FOREIGN KEY (stock_id) REFERENCES Stock (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE Gift DROP FOREIGN KEY FK_44E3633CD53EDB6');
        $this->addSql('ALTER TABLE Gift DROP FOREIGN KEY FK_44E3633DCD6110');
        $this->addSql('DROP TABLE Gift');
        $this->addSql('DROP TABLE Receiver');
        $this->addSql('DROP TABLE Stock');
    }
}
