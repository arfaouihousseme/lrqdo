<?php

namespace App\Model;

class File
{
    /** @var array */
    private $columns;

    /** @var Line[] */
    private $lines;

    /** @var FileReport */
    private $report;

    public function __construct()
    {
        $this->columns = [];
        $this->lines = [];
        $this->report = new FileReport();
    }

    public function getColumns(): array
    {
        return $this->columns;
    }

    public function setColumns(array $columns): self
    {
        $this->columns = $columns;

        return $this;
    }

    public function getLines(): array
    {
        return $this->lines;
    }

    public function setLines(array $lines): self
    {
        $this->lines = $lines;

        return $this;
    }

    public function addLine(Line $line): self
    {
        $this->lines[] = $line;

        return $this;
    }

    public function getReport(): FileReport
    {
        return $this->report;
    }

    public function setReport(FileReport $report): self
    {
        $this->report = $report;

        return $this;
    }
}
