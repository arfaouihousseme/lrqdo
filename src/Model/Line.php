<?php

namespace App\Model;

class Line
{
    /** @var string */
    private $giftUuid = null;

    /** @var string */
    private $giftCode = null;

    /** @var string */
    private $giftDescription = null;

    /** @var string */
    private $giftPrice = null;

    /** @var string */
    private $receiverUuid = null;

    /** @var string */
    private $receiverLastName = null;

    /** @var string */
    private $receiverFirstName = null;

    /** @var string */
    private $receiverCountryCode = null;

    public function __construct(array $data, array $columns)
    {
        $giftUuiIndex = array_search('gift_uuid', $columns);
        $giftCodeIndex = array_search('gift_code', $columns);
        $giftDescriptionIndex = array_search('gift_description', $columns);
        $giftPriceIndex = array_search('gift_price', $columns);
        $ReceiverUuiIndex = array_search('receiver_uuid', $columns);
        $ReceiverLastNameIndex = array_search('receiver_last_name', $columns);
        $ReceiverFirstNameIndex = array_search('receiver_first_name', $columns);
        $ReceiverCountryCodeIndex = array_search('receiver_country_code', $columns);

        if (false !== $giftUuiIndex && array_key_exists($giftUuiIndex, $data) && $data[$giftUuiIndex]) {
            $this->giftUuid = $data[$giftUuiIndex];
        }

        if (false !== $giftCodeIndex && array_key_exists($giftCodeIndex, $data) && $data[$giftCodeIndex]) {
            $this->giftCode = $data[$giftCodeIndex];
        }

        if (false !== $giftDescriptionIndex && array_key_exists($giftDescriptionIndex, $data) && $data[$giftDescriptionIndex]) {
            $this->giftDescription = $data[$giftDescriptionIndex];
        }

        if (false !== $giftPriceIndex && array_key_exists($giftPriceIndex, $data) && $data[$giftPriceIndex]) {
            $this->giftPrice = $data[$giftPriceIndex];
        }

        if (false !== $ReceiverUuiIndex && array_key_exists($ReceiverUuiIndex, $data) && $data[$ReceiverUuiIndex]) {
            $this->receiverUuid = $data[$ReceiverUuiIndex];
        }

        if (false !== $ReceiverLastNameIndex && array_key_exists($ReceiverLastNameIndex, $data) && $data[$ReceiverLastNameIndex]) {
            $this->receiverLastName = $data[$ReceiverLastNameIndex];
        }

        if (false !== $ReceiverFirstNameIndex && array_key_exists($ReceiverFirstNameIndex, $data) && $data[$ReceiverFirstNameIndex]) {
            $this->receiverFirstName = $data[$ReceiverFirstNameIndex];
        }

        if (false !== $ReceiverCountryCodeIndex && array_key_exists($ReceiverCountryCodeIndex, $data) && $data[$ReceiverCountryCodeIndex]) {
            $this->receiverCountryCode = $data[$ReceiverCountryCodeIndex];
        }
    }

    public function getGiftUuid()
    {
        return $this->giftUuid;
    }

    public function setGiftUuid($giftUuid): void
    {
        $this->giftUuid = $giftUuid;
    }

    public function getGiftCode()
    {
        return $this->giftCode;
    }

    public function setGiftCode($giftCode): void
    {
        $this->giftCode = $giftCode;
    }

    public function getGiftDescription()
    {
        return $this->giftDescription;
    }

    public function setGiftDescription($giftDescription): void
    {
        $this->giftDescription = $giftDescription;
    }

    public function getGiftPrice()
    {
        return $this->giftPrice;
    }

    public function setGiftPrice($giftPrice): void
    {
        $this->giftPrice = $giftPrice;
    }

    public function getReceiverUuid()
    {
        return $this->receiverUuid;
    }

    public function setReceiverUuid($receiverUuid): void
    {
        $this->receiverUuid = $receiverUuid;
    }

    public function getReceiverLastName()
    {
        return $this->receiverLastName;
    }

    public function setReceiverLastName($receiverLastName): void
    {
        $this->receiverLastName = $receiverLastName;
    }

    public function getReceiverFirstName()
    {
        return $this->receiverFirstName;
    }

    public function setReceiverFirstName($receiverFirstName): void
    {
        $this->receiverFirstName = $receiverFirstName;
    }

    public function getReceiverCountryCode()
    {
        return $this->receiverCountryCode;
    }

    public function setReceiverCountryCode($receiverCountryCode): void
    {
        $this->receiverCountryCode = $receiverCountryCode;
    }

    public function isValid(): bool
    {
        return $this->giftUuid &&
               $this->giftCode &&
               $this->giftPrice &&
               $this->receiverUuid &&
               $this->receiverFirstName &&
               $this->receiverLastName &&
               $this->receiverCountryCode;
    }
}
