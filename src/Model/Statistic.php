<?php

namespace App\Model;

class Statistic
{
    /** @var string */
    private $stockUuid;

    /** @var int */
    private $giftCount;

    /** @var int */
    private $countryCount;

    /** @var float */
    private $averagePrice;

    /** @var float */
    private $minPrice;

    /** @var float */
    private $maxPrice;

    public function __construct(
        string $stockUuid,
        int $giftCount,
        int $countryCount,
        float $averagePrice,
        float $minPrice,
        float $maxPrice
    ) {
        $this->stockUuid = $stockUuid;
        $this->giftCount = $giftCount;
        $this->countryCount = $countryCount;
        $this->averagePrice = round($averagePrice, 2);
        $this->minPrice = $minPrice;
        $this->maxPrice = $maxPrice;
    }

    public function setStockUuid(string $stockUuid): self
    {
        $this->stockUuid = $stockUuid;

        return $this;
    }

    public function setGiftCount(int $giftCount): self
    {
        $this->giftCount = $giftCount;

        return $this;
    }

    public function setCountryCount(int $countryCount): self
    {
        $this->countryCount = $countryCount;

        return $this;
    }

    public function setAveragePrice(float $averagePrice): self
    {
        $this->averagePrice = round($averagePrice, 2);

        return $this;
    }

    public function setMinPrice(float $minPrice): self
    {
        $this->minPrice = $minPrice;

        return $this;
    }

    public function setMaxPrice(float $maxPrice): self
    {
        $this->maxPrice = $maxPrice;

        return $this;
    }

    public function getStockUuid(): string
    {
        return $this->stockUuid;
    }

    public function getGiftCount(): int
    {
        return $this->giftCount;
    }

    public function getCountryCount(): int
    {
        return $this->countryCount;
    }

    public function getAveragePrice(): float
    {
        return $this->averagePrice;
    }

    public function getMinPrice(): float
    {
        return $this->minPrice;
    }

    public function getMaxPrice(): float
    {
        return $this->maxPrice;
    }
}
