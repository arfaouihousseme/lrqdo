<?php

namespace App\Model;

use App\Entity\Gift;

class FileReport
{
    /** @var array */
    private $errors;

    /** @var Gift[] */
    private $success;

    public function __construct()
    {
        $this->errors = [];
        $this->success = [];
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    public function setErrors(array $errors): self
    {
        $this->errors = $errors;

        return $this;
    }

    public function addError(string $error): self
    {
        $this->errors[] = $error;

        return $this;
    }

    public function cleanErrors(): self
    {
        $this->errors = [];

        return $this;
    }

    public function getSuccess(): array
    {
        return $this->success;
    }

    public function setSuccess(array $success): self
    {
        $this->success = $success;

        return $this;
    }

    public function addSuccess(Gift $gift): self
    {
        $this->success[] = $gift;

        return $this;
    }
}
