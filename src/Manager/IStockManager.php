<?php

namespace App\Manager;

use App\Entity\Stock;

interface IStockManager
{
    public function save(Stock $stock): Stock;

    public function create(): Stock;
}
