<?php

namespace App\Manager;

use App\Repository\StockRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Stock;
use Ramsey\Uuid\Uuid;

class StockManager implements IStockManager
{
    /** @var StockRepository */
    protected $repository;

    /** @var EntityManagerInterface */
    protected $em;

    public function __construct(StockRepository $repository, EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    public function save(Stock $stock): Stock
    {
        $this->em->persist($stock);
        $this->em->flush();

        return $stock;
    }

    public function create(): Stock
    {
        $stock = new Stock();

        $stock->setUuid(Uuid::uuid4());

        return $stock;
    }
}