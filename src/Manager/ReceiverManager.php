<?php

namespace App\Manager;

use App\Repository\ReceiverRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Receiver;
use App\Model\Line;

class ReceiverManager implements IReceiverManager
{
    /** @var ReceiverRepository */
    protected $repository;

    /** @var EntityManagerInterface */
    protected $em;

    public function __construct(ReceiverRepository $repository, EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    public function save(Receiver $receiver): Receiver
    {
        $this->em->persist($receiver);
        $this->em->flush();

        return $receiver;
    }

    public function create(Line $line): Receiver
    {
        $receiver = new Receiver();

        $receiver->setUuid($line->getReceiverUuid())
                 ->setFirstName($line->getReceiverFirstName())
                 ->setLastName($line->getReceiverLastName())
                 ->setCountryCode($line->getReceiverCountryCode());

        return $receiver;
    }
}
