<?php

namespace App\Manager\Import;

use App\Entity\Stock;
use App\Model\File;
use App\Validator\Import\Stock\IValidator;
use App\Manager\IReceiverManager;
use App\Manager\IStockManager;
use App\Manager\IGiftManager;

class ImportManager implements IImportManager
{
    /** @var IGiftManager */
    private $giftManager;

    /** @var IReceiverManager */
    private $ReceiverManager;

    /** @var IStockManager */
    private $stockManager;

    /** @var IValidator */
    private $csvValidator;

    /** @var Stock|null */
    private $currentStock = null;

    public function __construct(
        IGiftManager $giftManager,
        IReceiverManager $ReceiverManager,
        IStockManager $stockManager,
        IValidator $csvValidator
    ) {
        $this->giftManager = $giftManager;
        $this->ReceiverManager = $ReceiverManager;
        $this->stockManager = $stockManager;
        $this->csvValidator = $csvValidator;
    }

    public function importFile(File $fileContent): File
    {
        $lines = $fileContent->getLines();

        foreach ($lines as $line) {
            // skip invalid line
            if (!$line->isValid()) {
                continue;
            }

            $currentStock = $this->getCurrentStock();

            $gift = $this->giftManager->create($line, $currentStock);

            $this->giftManager->save($gift);

            $fileContent->getReport()->addSuccess($gift);
        }

        return $fileContent;
    }

    public function getCurrentStock(): Stock
    {
        if (null === $this->currentStock) {
            $stock = $this->stockManager->create();

            $this->currentStock = $this->stockManager->save($stock);
        }

        return $this->currentStock;
    }
}
