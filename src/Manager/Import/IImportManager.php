<?php

namespace App\Manager\Import;

use App\Model\File;
use App\Entity\Stock;

interface IImportManager
{
    public function importFile(File $fileContent): File;

    public function getCurrentStock(): Stock;
}
