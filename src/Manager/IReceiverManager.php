<?php

namespace App\Manager;

use App\Entity\Receiver;
use App\Model\Line;

interface IReceiverManager
{
    public function save(Receiver $receiver): Receiver;

    public function create(Line $line): Receiver;
}
