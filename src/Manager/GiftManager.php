<?php

namespace App\Manager;

use App\Repository\GiftRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Gift;
use App\Model\Line;
use App\Entity\Stock;

class GiftManager implements IGiftManager
{
    /** @var GiftRepository */
    protected $repository;

    /** @var EntityManagerInterface */
    protected $em;

    /** @var ReceiverManager */
    protected $receiverManager;

    public function __construct(
        GiftRepository $repository,
        EntityManagerInterface $em,
        ReceiverManager $receiverManager
    ) {
        $this->repository = $repository;
        $this->em = $em;
        $this->receiverManager = $receiverManager;
    }

    public function save(Gift $gift): Gift
    {
        $this->em->persist($gift);
        $this->em->flush();

        return $gift;
    }

    public function create(Line $line, Stock $stock): Gift
    {
        $gift = new Gift();
        $receiver = $this->receiverManager->create($line);

        $gift->setCode($line->getGiftCode())
             ->setDescription($line->getGiftDescription())
             ->setPrice($line->getGiftPrice())
             ->setUuid($line->getGiftUuid())
             ->setReceiver($receiver)
             ->setStock($stock);

        return $gift;
    }

    public function getStatistic(): array
    {
        return $this->repository->getStatitic();
    }
}