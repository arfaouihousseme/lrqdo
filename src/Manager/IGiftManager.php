<?php

namespace App\Manager;

use App\Entity\Gift;
use App\Entity\Stock;
use App\Model\Line;

interface IGiftManager
{
    public function save(Gift $gift): Gift;

    public function create(Line $line, Stock $stock): Gift;

    public function getStatistic(): array;
}
