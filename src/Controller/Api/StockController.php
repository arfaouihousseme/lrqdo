<?php

namespace App\Controller\Api;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Validator\Import\Stock\IValidator;
use App\Validator\Import\Stock\IFileParser;
use App\Manager\Import\IImportManager;

class StockController extends AbstractFOSRestController
{
    /**
     * @Rest\Post("/stock", name="stock_create")
     */
    public function createAction(
        Request $request,
        IValidator $csvValidator,
        IFileParser $csvParser,
        IImportManager $importManager
    ) {
        $contentFile = $csvParser->parse($request->getContent());

        if ($csvValidator->validate($contentFile)) {
            $contentFile = $importManager->importFile($contentFile);


            return $this->handleView($this->view([
                'current_uuid_stock' => $importManager->getCurrentStock()->getUuid(),
                'results' => [
                    'total_inserted' => count($contentFile->getReport()->getSuccess()),
                    'gifts' => $contentFile->getReport()->getSuccess(),
                ],
                'errors' => [
                    'total_skipped' => count($contentFile->getReport()->getErrors()),
                    'messages' => $contentFile->getReport()->getErrors()
                ]
            ]));
        }

        return $this->handleView($this->view([
            'errors' => $contentFile->getReport()->getErrors()
        ], Response::HTTP_BAD_REQUEST));
    }
}
