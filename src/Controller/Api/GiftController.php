<?php

namespace App\Controller\Api;

use App\Manager\IGiftManager;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

class GiftController extends AbstractFOSRestController
{
    /**
     * @Rest\Get("/gift/statistic", name="gift_statistic")
     */
    public function statisticAction(IGiftManager $giftManager)
    {
        $statistics = $giftManager->getStatistic();

        return $this->handleView($this->view([
            'statistics' => $statistics
        ]));
    }
}
