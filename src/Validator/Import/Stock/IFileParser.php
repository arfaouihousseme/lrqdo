<?php

namespace App\Validator\Import\Stock;

use App\Model\File;

interface IFileParser
{
    public function parse(string $content): File;
}