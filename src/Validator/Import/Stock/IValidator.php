<?php

namespace App\Validator\Import\Stock;

use App\Model\File;

interface IValidator
{
    public function validate(File $fileContent): bool;
}