<?php

namespace App\Validator\Import\Stock;

use App\Model\File;

class CsvValidator implements IValidator
{
    const ERROR_MISSING_COLUMNS = 'Missing(s) columns(s) : [ %s ], you must enter these columns [ %s ]';
    const ERROR_WRONG_COLUMNS = 'Wrong(s) columns(s) : [ %s ], you must enter only these columns [ %s ]';
    const ERROR_WRONG_LINE = 'Error on line number : %s, you must enter exactly these columns [ %s ] ';
    const SEPARATOR = ',';

    /** @var array */
    protected $requiredColumns;

    public function __construct(array $requiredColumns)
    {
        $this->requiredColumns = $requiredColumns;
    }

    public function validate(File $fileContent): bool
    {
        $hasValidHeader = $this->validateFileHeader($fileContent);

        // skip if header is KO
        if (!$hasValidHeader) {
            return false;
        }

        $this->validateFileLines($fileContent);

        return true;
    }


    private function validateFileHeader(File $fileContent): bool
    {
        $missingColumns = array_diff($this->requiredColumns, $fileContent->getColumns());

        if (!empty($missingColumns)) {
            $fileContent->getReport()->addError(
                sprintf(self::ERROR_MISSING_COLUMNS,
                    implode(self::SEPARATOR, $missingColumns),
                    implode(self::SEPARATOR, $this->requiredColumns)
                )
            );

            return false;
        }

        $wrongColumns = array_diff($fileContent->getColumns(), $this->requiredColumns);

        if (!empty($wrongColumns)) {
            $fileContent->getReport()->addError(
                sprintf(self::ERROR_WRONG_COLUMNS,
                    implode(self::SEPARATOR, $wrongColumns),
                    implode(self::SEPARATOR, $this->requiredColumns)
                )
            );

            return false;
        }

        return true;
    }

    private function validateFileLines(File $fileContent): void
    {
        foreach ($fileContent->getLines() as $index => $line) {
            if (!$line->isValid()) {
                $fileContent->getReport()->addError(
                    sprintf(self::ERROR_WRONG_LINE, 2 + $index, implode(self::SEPARATOR, $this->requiredColumns))
                );
            }
        }
    }
}
