<?php

namespace App\Validator\Import\Stock;

use App\Model\File;
use App\Model\Line;

class CsvParser implements IFileParser
{
    public function parse(string $content): File
    {
        $fileContent = new File();
        $lines = explode(PHP_EOL, $content);

        foreach ($lines as $key => $line) {
            $line = str_getcsv($line);

            if (0 === $key) {
                $fileContent->setColumns($line);
            } else {
                $fileContent->addLine(
                    new Line($line, $fileContent->getColumns())
                );
            }

        }

        return $fileContent;
    }
}
