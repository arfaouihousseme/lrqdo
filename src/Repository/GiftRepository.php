<?php

namespace App\Repository;

use App\Entity\Gift;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Model\Statistic;

/**
 * @method Gift|null find($id, $lockMode = null, $lockVersion = null)
 * @method Gift|null findOneBy(array $criteria, array $orderBy = null)
 * @method Gift[]    findAll()
 * @method Gift[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GiftRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Gift::class);
    }

    /**
    * @return Statistic[] Returns an array of Statistic objects
    */
    public function getStatitic()
    {
        $qb = $this->createQueryBuilder('g');

        $queryResult = $qb
                     ->select('g.uuid as stock_uuid,
                              count(g.id) as count_gift,
                              count(distinct(r.countryCode)) as country_count,
                              avg(g.price) as average_price,
                              min(g.price) as min_price,
                              max(g.price) as max_price'
                     )
                     ->innerJoin('g.receiver', 'r')
                     ->groupBy('g.stock')
                     ->getQuery()
                     ->getResult();

        return array_map(function ($statistic) {
            return new Statistic(
                $statistic['stock_uuid'],
                $statistic['count_gift'],
                $statistic['country_count'],
                $statistic['average_price'],
                $statistic['min_price'],
                $statistic['max_price']
            );
        }, $queryResult);
    }
}
