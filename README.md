# Lrqdo

setup project 

docker-compose -f docker-compose-dev.yml up -d

docker exec -it lrqdo-php-fpm "bin/console doctrine:migrations:migrate"

to import stock : 

url : http://127.0.0.1:1088/api/stock
method : post
body: stock.csv (file)

to get statistics :

url : http://127.0.0.1:1088/api/gift/statistic
method : get
